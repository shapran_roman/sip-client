src_ip = ""
dst_ip = ""
dst_port = ""


class Client(src_ip, dst_ip, dst_port):
    invite = f"""
        INVITE sip:55555@{dst_ip}:{dst_port} SIP/2.0
        Via: SIP/2.0/UDP {src_ip};rport;branch=z9hG4bK52HNj0Sj07QDD
        Max-Forwards: 68
        From: "972541028402" <sip:972541028402@80.87.198.78>;tag=2QjUp3yrHNSZF
        To: <sip:55555@80.87.198.78:5065>
        Call-ID: f856b917-d724-1238-cba8-0ac7530c3fcf
        CSeq: 17022435 INVITE
        Contact: <sip:gw+freecell@35.176.18.191:5060;transport=udp;gw=freecell>
        Allow: INVITE, ACK, BYE, CANCEL, OPTIONS, MESSAGE, INFO, UPDATE, REGISTER, REFER, NOTIFY
        Supported: timer, path, replaces
        Allow-Events: talk, hold, conference, refer
        Content-Type: application/sdp
        Content-Disposition: session
        Content-Length: 222
    """

    register = f""



    """
        INVITE sip:55555@{dst_ip}:{dst_port} SIP/2.0
        Via: SIP/2.0/UDP {src_ip};rport;branch=z9hG4bK52HNj0Sj07QDD
        Max-Forwards: 68
        From: "972541028402" <sip:972541028402@80.87.198.78>;tag=2QjUp3yrHNSZF
        To: <sip:55555@80.87.198.78:5065>
        Call-ID: f856b917-d724-1238-cba8-0ac7530c3fcf
        CSeq: 17022435 INVITE
        Contact: <sip:gw+freecell@35.176.18.191:5060;transport=udp;gw=freecell>
        User-Agent: FreeSWITCH-mod_sofia/1.6.20~64bit
        Allow: INVITE, ACK, BYE, CANCEL, OPTIONS, MESSAGE, INFO, UPDATE, REGISTER, REFER, NOTIFY^M
        Supported: timer, path, replaces
        Allow-Events: talk, hold, conference, refer
        Content-Type: application/sdp
        Content-Disposition: session
        Content-Length: 222"""