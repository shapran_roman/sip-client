from socket import *
import sys

host = '127.0.0.1'
port = 5060
addr = (host, port)

udp_socket = socket(AF_INET, SOCK_DGRAM)

data = """
INVITE sip:55555@127.0.0.1:5060 SIP/2.0
Via: SIP/2.0/UDP 127.0.0.1;rport;branch=z9hG4bK52HNj0Sj07QDD
Max-Forwards: 68
From: "11111" <sip:972541028402@127.0.0.1>;tag=2QjUp3yrHNSZF
To: <sip:55555@127.0.0.1:5065>
Call-ID: f856b917-d724-1238-cba8-0ac7530c3fcf
CSeq: 17022435 INVITE
Contact: <sip:gw+freecell@127.0.0.1:5060;transport=udp;gw=freecell>
Allow: INVITE, ACK, BYE, CANCEL, OPTIONS, MESSAGE, INFO, UPDATE, REGISTER, REFER, NOTIFY
"""

udp_socket.sendto(data.encode(), addr)
udp_socket.close()